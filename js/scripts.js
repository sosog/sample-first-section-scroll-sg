
$(document).ready(function(){
    sectionScrollModule().init();
    
});

var sectionScrollModule = (function(){
    var body =  $('html,body')
    var sectionOffset = $('.section-scroll-js').offset();
    var downBlocker = true;
    var upBlocker = false;
    var lastScrollTop = 0;

    function init(){

    }
    
    function downScroll(that){
        
        if (that.scrollTop() > 5 && that.scrollTop() < 200 && downBlocker == true){
            downBlocker = false;
            body.animate({scrollTop:sectionOffset.top},{
                duration: 500,
                specialEasing: {
                    height: "ease"
                }
            });
                
            setTimeout(function() {
                downBlocker = true;
                upBlocker = true;
            }, 510);
        }
    }
    function upScroll(that){
        if (that.scrollTop() < (sectionOffset.top - 100) && upBlocker == true){
            upBlocker = false;
            body.animate({scrollTop:0},{
                duration: 500,
                specialEasing: {
                    height: "ease"
                }
            });
            
            setTimeout(function() {
                upBlocker = true;
                upBlocker = false;
            }, 510);
        }
    }

    $(window).scroll(function(event){
        var st = $(this).scrollTop();
        if (st > lastScrollTop){
             // downscroll code
           downScroll($(this)); 
        } 
        else {
            // upscroll code
            upScroll($(this));
        }
        lastScrollTop = st;
     });


    return {
        init: function(){
            return init();
        }
    }
});